import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {AppRoutingModule} from './app.routing.module';
import {RouterModule} from '@angular/router';
import { NgxEditorModule } from 'ngx-editor';
import {RatingModule} from "ngx-rating";
import {EditorModule} from 'primeng/editor';
import {ProgressBarModule} from 'primeng/progressbar';
import {AccordionModule} from 'primeng/accordion';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import {TooltipModule} from 'primeng/tooltip';
import {
  MatButtonModule,
   MatCheckboxModule,
   MatTooltipModule,
   MatInputModule, 
   MatSelectModule,
   MatPaginatorModule, 
   MatProgressSpinnerModule, 
  MatSortModule,
   MatTableModule,
   MatGridListModule,
   MatCardModule,
   MatDatepickerModule,
   MatNativeDateModule,
   MatToolbarModule,
   MatMenuModule,
   MatIconModule,
   MatListModule,
   MatDialogModule
  } from '@angular/material';

import { AppComponent } from './app.component';
import {JobDescriptionComponent,SpeechRecognitionService} from './jobs';
import { JobListComponent } from './components/jobs/joblist';
import { jqxBarGaugeComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxbargauge';
import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { DataTableComponent } from './components/data-table/data-table.component';
import { JobService } from './components/jobs/joblist/JobService';
import { AuthenticationService } from './components/authentication/authentication.service';
import { PopoverModule } from 'ngx-bootstrap';
import { JobDescriptionService } from './jobs/JobDescriptionService';
import { DeleteJobDialog } from './components/jobs/joblist/DeleteJobDialog';
import { AuthenticationGaurd } from './components/authentication/authentication.gaurd';
import { UnAuthorizedComponent } from './components/authentication/unauthorized/unauthorized.component';



@NgModule({
  declarations: [
    AppComponent,
    JobDescriptionComponent,
    jqxBarGaugeComponent,
    jqxGridComponent,
    JobListComponent,
    DataTableComponent,
    DeleteJobDialog,
    UnAuthorizedComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NgxEditorModule,
    EditorModule,
    ProgressBarModule,
    AccordionModule,
    RatingModule,
    TooltipModule,
    MatTooltipModule,
    MatCardModule,
    MatButtonModule,
     MatCheckboxModule,
     MatTableModule,
     MatPaginatorModule,
     MatInputModule,
     MatSelectModule,
     MatDatepickerModule,
     MatNativeDateModule,
     MatToolbarModule,
     MatGridListModule,
     MatMenuModule,
     MatIconModule,
     MatListModule,
     MatDialogModule,
     MatProgressSpinnerModule,
     MatSortModule,
    AngularMultiSelectModule,
    PopoverModule.forRoot()
  ],
  providers: [
    SpeechRecognitionService,
    JobService,
    AuthenticationService,
    JobDescriptionService,
    AuthenticationGaurd
  ],
  bootstrap: [AppComponent],
  entryComponents:[DeleteJobDialog]
})
export class AppModule { }

