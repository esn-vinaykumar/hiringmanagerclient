import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { JobDescriptionComponent } from './jobs'
import { JobListComponent } from './components/jobs/joblist';
import { AuthenticationGaurd } from './components/authentication/authentication.gaurd';
import { UnAuthorizedComponent } from './components/authentication/unauthorized/unauthorized.component';
const routes: Routes = [
    { path: 'editjobdescription/:id/:state', component: JobDescriptionComponent,data: { authenticated: true },canActivate:[AuthenticationGaurd]  },
    { path: 'createjobdescription', component: JobDescriptionComponent,data: { authenticated: true },canActivate:[AuthenticationGaurd]    },
    { path: 'jobgridlist', component: JobListComponent,data: { authenticated: true },canActivate:[AuthenticationGaurd]  },
    {path:'unauthorizedaccess',component:UnAuthorizedComponent,data:{authenticated:false}},
    {
        path: 'login',
        loadChildren:'./components/authentication/authentication.module#AuthenticationModule'
    },
    {
        path: 'user',
        canActivate:[AuthenticationGaurd],
        loadChildren:'./components/users/user.module#UserModule'
    },
    {
        path: 'jobs',
        canActivate:[AuthenticationGaurd], 
        loadChildren:'./components/jobs/companyjobs.module#JobsModule'
    },
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: '**', redirectTo: 'login'}
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ]
})
export class AppRoutingModule { }