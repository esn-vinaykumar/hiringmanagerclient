import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { AuthenticationService } from './components/authentication/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'hiring-manager';
  authenticated:boolean;
  loggedDetails:any;
  model:any;
  content = 'Vivamus sagittis lacus vel augue laoreet rutrum faucibus.';
  constructor(private activatedRoute:ActivatedRoute,
    private router:Router,
  private authService:AuthenticationService){

   this.model={
    ProfileImage:null
   }
  }
  ngOnInit():void{
    this.router.events
    .filter((event) => event instanceof NavigationEnd)
    .map(() => this.activatedRoute)
    .map((route) => {
      while (route.firstChild) route = route.firstChild;
      return route;
    })
    .filter((route) => route.outlet === 'primary')
    .mergeMap((route) => route.data)
    .subscribe((event) => {
      this.authenticated=event['authenticated'];
      this.loggedDetails=this.authService.getLoggedInfo();
      if(this.loggedDetails){
        var payLoad={
          id:this.loggedDetails.id
        }
        this.authService.findByUserId(payLoad).subscribe(data =>{
          this.model=data;
      })
      }
     
   //alert(JSON.stringify(this.loggedDetails));
    });
  }
  
}
