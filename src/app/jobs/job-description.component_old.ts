import { Component } from "@angular/core";

@Component({
selector:'job-description',
templateUrl:'./job-description.component.html',
styleUrls:['./job-description.component.scss']
})
export class JobDescriptionComponent{
    jobSuggestion:string;
    starsCount: number;
    isSuggestion:boolean=false;
    isSuggestionBtn:boolean=false;
    isSearchGrid:boolean=false;
    analyzeCount=0;
    resumeSourcesList:any=[];

    constructor(){
        //this.jobDescription='<b>dynamic content</b>';
        this.jobSuggestion=`<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Title:</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Job overview</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size:13px;color:#494949;background:white;">A great job description starts with a compelling summary of the position and its role within your company. Your summary should provide an overview of your company and expectations for the position. Outline the types of activities and responsibilities required for the job so job seekers can determine if they are qualified, or if the job is a good fit.</span></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Job responsibilities &amp; duties:</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size:13px;color:#494949;background:white;">outline the functions this position will perform on a regular basis, how the job functions within the organization and who the job reports to.</span></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Qualifications &amp; Skills:</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><span style="font-size:13px;color:#494949;background:white;">Outline the required and preferred skills for your position. Include soft skills and personality traits that you envision for a successful hire. Keep your list of qualifications concise, but provide enough detail with relevant keywords and terms.</span></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Skills:</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Required previous experiences:</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Soft skills</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Personality traits:</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Years of experience</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Education</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Certifications</span></strong></p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'>
	<br>
</p>

<p style='margin:0in;margin-bottom:.0001pt;font-size:15px;font-family:"Calibri",sans-serif;'><strong><span style="font-size:13px;">Top three skills</span></strong></p>`;
        this.resumeSourcesList=[
            {
                id:1,
                domain:"Everest Inc",
                resume:"Roselyne.docx",
                postAds:false,
                emailResumes:false

            },
            {
                id:1,
                domain:"Google Jobs",
                resume:"Norman Ralph.docx",
                postAds:false,
                emailResumes:false
            },
            {
                id:1,
                domain:"LinkedIn",
                resume:"Hans Christian.docx",
                postAds:false,
                emailResumes:false
            },
            {
                id:1,
                domain:"Monster",
                resume:"Alphonse.docx",
                postAds:false,
                emailResumes:false
            },
            {
                id:1,
                domain:"Dice",
                resume:"Leone Battista.docx",
                postAds:false,
                emailResumes:false
            }
        ];
    }

    public analyze(){
        //alert(this.jobDescription)
        this.isSearchGrid=false;
        this.analyzeCount++
        //this.starsCount=5;
        this.starsCount=3;
        if(this.analyzeCount == 1){
            this.isSuggestion=true;
            this.isSuggestionBtn=true;
        }
        else if(this.analyzeCount > 1){
            this.isSuggestionBtn=true;
            this.isSuggestion=false;
            this.starsCount=5;
        }
        
    }
    public search(){
        this.isSearchGrid=true;
    }
    public postSelectedAds(item,selected){
        if(selected){
            item.postAds=true;
        }
        else {
            item.postAds=false;
        }
        
        //alert(JSON.stringify(item));

    }
    public postSelectedEmails(item,selected){
        //alert(JSON.stringify(item));
        if(selected){
            item.emailResumes=true;
        }
        else {
            item.emailResumes=false;
        }
    }

}