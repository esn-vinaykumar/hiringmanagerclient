import { Component, OnInit, OnDestroy } from "@angular/core";
import { SpeechRecognitionService } from './SpeechRecognitionService';

import { SelectorMatcher } from "@angular/compiler";
import { ActivatedRoute, Params, Route, Router } from "@angular/router";
import { IJobDescription } from "./IJobDescription";
import { JobDescription } from "./JobDescription";
import { JobDescriptionService } from "./JobDescriptionService";
import { AuthenticationService } from "../components/authentication/authentication.service";
import { MatDialog } from "@angular/material";
import { DeleteJobDialog } from "../components/jobs/joblist/DeleteJobDialog";

@Component({
    selector: 'job-description',
    templateUrl: './job-description.component.html',
    styleUrls: ['./job-description.component.scss']
})
export class JobDescriptionComponent implements OnInit {
    
    jobGridData: any = [];
    fieldPreviewMode: boolean = false;
    saveVisible:boolean=false;
    isShowSampleResumes: boolean = false;
    itemList = [];
    starsCount: number = 5;
    selectedItems = [];
    settings = {};
    showSearchButton: boolean;
    speechData: string;
    progressNumber: number = 0;
    isPostAds: boolean = false;
    isPostEmails: boolean = false;
    jobDetails: any = {};
    index: number = 0;
    resumeSourcesList: any = [];
    sampleResumesList: any = [];
    isSearchList: boolean = false;
    selectedJobId: number;
    model: IJobDescription;
    loggedInfo:any;


    constructor(private speechRecognitionService: SpeechRecognitionService,
        private activatedRoute: ActivatedRoute,
        private router:Router,
        
        private jobService: JobDescriptionService,
        private authService: AuthenticationService) {
        this.model = new JobDescription();
        this.showSearchButton = true;
        this.speechData = "";

        this.fieldPreviewMode = false;


        this.activatedRoute.params.subscribe((params: Params) => {

            let id = params['id'];
            var payLoad = {
                id: id + ""
            }
            if (params['state'] == 'view') {
                this.fieldPreviewMode = false;
                this.jobService.findJobById(payLoad)
                    .subscribe(data => {
                        this.model = data;
                    })
            }
            else if (params['state'] == 'edit') {
                this.fieldPreviewMode = true;
                this.saveVisible=false;
                this.jobService.findJobById(payLoad)
                    .subscribe(data => {
                        this.model = data;
                    })
            }
            else {
                this.fieldPreviewMode = true;
                 this.saveVisible=true;
            }

        });

    }
    ngOnInit() {
        this.loggedInfo=this.authService.getLoggedInfo();

    }
    onItemSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }
    OnItemDeSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }
    onSelectAll(items: any) {
        console.log(items);
    }
    onDeSelectAll(items: any) {
        console.log(items);
    }

    ngOnDestroy() {
        this.speechRecognitionService.DestroySpeechObject();
    }

    activateSpeechSearchMovie(): void {
        this.showSearchButton = false;

        this.speechRecognitionService.record()
            .subscribe(
                //listener
                (value) => {
                    this.speechData = value;
                    console.log(value);
                },
                //errror
                (err) => {
                    console.log(err);
                    if (err.error == "no-speech") {
                        console.log("--restatring service--");
                        this.activateSpeechSearchMovie();
                    }
                },
                //completion
                () => {
                    this.showSearchButton = true;
                    console.log("--complete--");
                    this.activateSpeechSearchMovie();
                });
    }

    openNext() {
        this.index = (this.index === 2) ? 0 : this.index + 1;
    }

    openPrev() {
        this.index = (this.index === 0) ? 2 : this.index - 1;
    }
    makeSpeechRecognition() {
        alert();
    }
    search() {
        this.isSearchList = true;
    }
    clear() {

    }
    onTabOpen() {
        //this.progressNumber=this.progressNumber+30
        //alert('tabopen');
        this.starsCount = 5;
    }
    changeProgressPercent() {
        this.progressNumber = Math.round(this.progressNumber + 6.25);
    }
    postSelectedAds(item, selected) {
        this.isPostAds = true;
        this.isPostEmails = false;
        if (selected) {
            item.postAds = true;
        }
        else {
            item.postAds = false;
        }

        //alert(JSON.stringify(item));

    }
    postSelectedEmails(item, selected) {
        //alert(JSON.stringify(item));
        this.isPostAds = false;
        this.isPostEmails = true;
        if (selected) {
            item.emailResumes = true;
        }
        else {
            item.emailResumes = false;
        }
    }
    viewResumeSamples() {
        this.isShowSampleResumes = true;
    }
    editMode() {
        this.fieldPreviewMode = true;
        
    }
    viewMode() {
        this.fieldPreviewMode = false;
    }
    updateJob(){
        this.model.ModifiedBy=this.loggedInfo.id;
        this.model.ModifiedDate=new Date();
        //this.model.JobStatus="Open";
        this.jobService.updateJob(this.model)
            .subscribe((result) => {
                this.router.navigate(['/jobgridlist']);

            },
                error => {
                    console.log(error.message)
                })
    }
    
    
    saveJob() {
        this.model.UserId=this.loggedInfo.id;
        this.model.JobStatus="Open";
        this.jobService.saveJob(this.model)
            .subscribe((result) => {
                this.router.navigate(['/jobgridlist']);
            },
                error => {
                    console.log(error.message)
                })
    }



}