import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { HttpConstants } from "../constants/Http.constants";

@Injectable()
export class JobDescriptionService{
    public apiUrl:string=HttpConstants.SERVER_API_ENDPOINT;

    constructor(private httpClient:HttpClient){
        
    }

    updateJob(payLoad:any){
        return this.httpClient.post(this.apiUrl+'/api/job/updatejob',payLoad,{})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
    saveJob(payLoad:any){
        return this.httpClient.post(this.apiUrl+'/api/job/save',payLoad,{})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
    getAllJobs():Observable<any>{
        return this.httpClient.get(this.apiUrl+'/api/job/getalljobs')
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
    getUserJobs(payLoad:any):Observable<any>{
        return this.httpClient.get(this.apiUrl+'/api/job/getuserjobs',{params: payLoad})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
    getJobDateRange(payLoad:any):Observable<any>{
        return this.httpClient.get(this.apiUrl+'/api/job/getjobdaterange',{params: payLoad})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
    findJobById(payLoad:any):Observable<any>{
        return this.httpClient.get(this.apiUrl+'/api/job/findbyjob',{params: payLoad})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
    deleteJobById(payLoad:any):Observable<any>{
        return this.httpClient.get(this.apiUrl+'/api/job/deleteJob',{params: payLoad})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
}