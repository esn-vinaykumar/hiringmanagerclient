import { IJobDescription } from "./IJobDescription";

export class JobDescription implements IJobDescription{
    ID:number;
    UserId:number;
    JobID:string;
    JobStatus:string;
    CompanyName:string;
    Title:string;
    Overview:string;
    Responsibilities:string;
    TechnicalSkills:string;
    SoftSkills:string;
    Experience:string;
    PreviousExperience:string;
    Education:string;
    Certifications:string;
    AdditionalInformation:string;
    Benefits:string;
    HowToApply:string;
    Keywords:string;
    CreatedBy:string;
    CreatedDate:Date=new Date();
    ModifiedBy:string;
    ModifiedDate:Date;
}