import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../authentication.service";
import { IProfile } from "./IProfile";
import { Profile } from "./profile";

@Component({
    selector: 'profile-component',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss']

})
export class ProfileComponent implements OnInit {
    loggedUser: any;
    model: IProfile;
    constructor(
        private authService: AuthenticationService
    ) {
        this.model = new Profile();
    }
    ngOnInit() {
        this.loggedUser = this.authService.getLoggedInfo();
        var payLoad={
            id:this.loggedUser.id
        }
        this.authService.findByUserId(payLoad).subscribe(data =>{
            this.model=data;
        })

    }
    submit():void{

        this.authService.updateUser(this.model).subscribe(data =>{
        
        })
    }
    onFileInput(event){
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
      
            reader.readAsDataURL(event.target.files[0]); // read file as data url
      
            reader.onload = (event:any) => { // called once readAsDataURL is completed
              this.model.ProfileImage = event.target.result;
            }
          }
    }
}