import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild } from "@angular/router";
import { AuthenticationService } from "./authentication.service";

@Injectable()
export class AuthenticationGaurd implements CanActivate,CanActivateChild {

  constructor(private auth: AuthenticationService, private router: Router) {

  }
  canActivate(): boolean {
    if (!this.auth.isAuthenticated()) {
      console.log('you are not authorized to view this page.');
      this.router.navigate(['/unauthorizedaccess']);
      return false;
    }
    return true;

  }
  canActivateChild(): boolean {
    if (!this.auth.isAuthenticated()) {
      console.log('you are not authorized to view this page.');
      this.router.navigate(['/unauthorizedaccess']);
      return false;
    }
    return true;

  }
}