import {Routes} from '@angular/router'
import { LoginComponent } from './login';
import { ResetPasswordComponent } from './reset-password';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterComponent } from './register';
import { ProfileComponent } from './profile/profile.component';
export const routes: Routes = [
   {path: '',component: LoginComponent,data: { authenticated: false }},
    {path: 'resetpassword', component: ResetPasswordComponent,data: { authenticated: false }},
    {path: 'forgotpassword', component: ForgotPasswordComponent,data: { authenticated: false }},
    {path: 'register', component: RegisterComponent,data: { authenticated: false }},
    {path: 'profile', component: ProfileComponent,data: { authenticated: true }},
  ];