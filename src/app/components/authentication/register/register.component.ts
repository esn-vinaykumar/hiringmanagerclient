import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { IRegister } from "./IRegister";
import { Register } from "./Register";
import { AuthenticationService } from "../authentication.service";

export interface JobRolesList {
    value: string;
    viewValue: string;
}
@Component({
    selector: 'register-component',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
    model: IRegister;
    Roles: JobRolesList[] = [
        { value: 'Hiring Manager', viewValue: 'Hiring Manager' },
        { value: 'Interviewer', viewValue: 'Interviewer' },
        { value: 'HR Manager', viewValue: 'HR Manager' },
        { value: 'Recruiter', viewValue: 'Recruiter' },
        { value: 'Candidate', viewValue: 'Candidate' },
        { value: 'Administrator', viewValue: 'Administrator' },
    ];
    constructor(private router: Router, private authService: AuthenticationService) {
        this.model = new Register();
    }

    signUp(): void {
        this.authService.saveUser(this.model)
            .subscribe((result) => {
                this.router.navigate(['/login']);
            },
                error => {
                    console.log(error.message)
                },
                () => {
                    this.router.navigate(['/login']);
                })
                this.router.navigate(['/login']);

    }
    cancel(): void {
        this.router.navigate(['/login']);
    }
}