import { IRegister } from "./IRegister";
import { isDaylightSavingTime } from "ngx-bootstrap/chronos/units/offset";

export class Register implements IRegister {

    FirstName:string;
    LastName:string;
    Email:string;
    Mobile:string;
    Password:string;
    CreatedBy:string;
    CreatedDate:Date=new Date();
    ModifiedBy:string;
    ModifiedDate:string;
    Role:Date;

    fromJson(json: any): Register {
        const register = new Register();
        register.FirstName=json.FirstName;
        register.LastName=json.LastName;
        register.Email=json.Email;
        register.Mobile=json.Mobile;
        register.Password=json.Password;
        register.CreatedBy=json.CreatedBy;
        register.CreatedDate=json.CreatedDate;
        register.ModifiedBy=json.ModifiedBy;
        register.ModifiedDate=json.ModifiedDate;
        register.Role=json.Role;
    
    
        return register;
      }
    
      toJson(register: Register): any {
        return {
          FirstName:register.FirstName,
          LastName:register.LastName,
          Email:register.Email,
          Mobile:register.Mobile,
          Password:register.Password,
          CreatedBy:register.CreatedBy,
          CreatedDate:register.CreatedDate,
          ModifiedBy:register.ModifiedBy,
          ModifiedDate:register.ModifiedDate,
          Role:register.Role
        };
      }

}