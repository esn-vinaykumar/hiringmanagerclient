import { registerContentQuery } from "@angular/core/src/render3/instructions";

export interface IRegister{
    FirstName:string,
    LastName:string,
    Email:string,
    Mobile:string,
    Password:string,
    CreatedBy:string,
    CreatedDate:Date,
    ModifiedBy:string,
    ModifiedDate:string,
    Role:Date


}