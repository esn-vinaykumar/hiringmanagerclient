import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
    selector: 'reset-password-component',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent {
    constructor(private router: Router) {

    }
    cancel(): void {
        this.router.navigate(['/login']);
    }
    resetPassword(): void {
        this.router.navigate(['/login']);
    }
}