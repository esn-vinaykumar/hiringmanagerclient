import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { FormGroup } from "@angular/forms";
import { ILogin } from "./ILogin";
import { Login } from "./Login";
import { AuthenticationService } from "../authentication.service";

@Component({
    selector: 'login-component',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    model: ILogin;
    authenticationToken:any;
    loggedInfo:any;
    ngOnInit() {

    }
    constructor(private router: Router, private authService: AuthenticationService) {
        this.model = new Login();

    }
    login(): void {
        this.authService.validateUser(this.model)
            .subscribe((result) => {
                //console.log(JSON.stringify(result));
                this.loggedInfo=result;
                this.authService.storeLoggedInfo(this.loggedInfo);
                this.authenticationToken=result;

            },
                error => {
                    console.log(error.message);
                },
                () => {

                    this.authService.storeToken(this.authenticationToken.access_token);
                    if(this.loggedInfo.Role == "Administrator"){
                        this.router.navigate(['/user']);
                    }
                    else {
                        this.router.navigate(['/jobgridlist']);
                    }
                    

                })




        
    }
}