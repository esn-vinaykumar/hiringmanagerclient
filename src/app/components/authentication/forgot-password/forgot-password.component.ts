import { Component } from "@angular/core";
import { Router } from "@angular/router";

@Component({
selector:'forgot-password-component',
templateUrl:'./forgot-password.component.html',
styleUrls:['./forgot-password.component.scss']
})
export class ForgotPasswordComponent{
    constructor(private router:Router){

    }
    cancel():void{
        this.router.navigate(['/login']);

    }
    forgotPassword():void{
        this.router.navigate(['/login/resetpassword']);
    }
}