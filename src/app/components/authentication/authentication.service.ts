import { Injectable } from "@angular/core";
import { HttpHeaders,HttpClient } from "@angular/common/http";
import { catchError, map } from "rxjs/operators";
import { ILogin } from "./login/ILogin";
import { HttpConstants } from "../../constants/Http.constants";
import { Observable } from "rxjs";

@Injectable()
export class AuthenticationService{
    public apiUrl:string=HttpConstants.SERVER_API_ENDPOINT;
    constructor(private httpClient:HttpClient){
        
    }
    validateUser(user:ILogin){
        var userData="UserName="+user.UserName+"&password="+user.Password+"&grant_type=password";
        var reqHeader=new HttpHeaders({'Content-Type':'application/x-www-form-urlencoded'});
        return this.httpClient.post(this.apiUrl+'/token',userData,{headers:reqHeader})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
        
    }
    public isAuthenticated():boolean{
        return this.getToken()!=null;
    }
    storeToken(token:string){
        localStorage.setItem("token",token);
    }
    storeLoggedInfo(item:string){
        localStorage.setItem("loggedinfo",JSON.stringify(item));
    }
    getLoggedInfo(){
        return JSON.parse(localStorage.getItem("loggedinfo"));
    }
    removeLoggedInfo(){
        return localStorage.removeItem("loggedinfo");
    }
   
    getToken(){
        return localStorage.getItem("token");
    }
    removeToken(){
        return localStorage.removeItem("token");
    }
    saveUser(payLoad:any){
        return this.httpClient.post(this.apiUrl+'/api/users/save',payLoad,{})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
    findByUserId(payLoad:any):Observable<any>{
        return this.httpClient.get(this.apiUrl+'/api/users/findbyuserid',{params: payLoad})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
    updateUser(payLoad:any){
        return this.httpClient.post(this.apiUrl+'/api/users/updateuser',payLoad,{})
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }

}