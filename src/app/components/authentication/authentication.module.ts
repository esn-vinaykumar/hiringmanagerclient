import { NgModule } from "@angular/core";
import { AuthenticationRoutingModule } from "./authentication.routing.module";
import { LoginComponent } from "./login";
import { ForgotPasswordComponent } from "./forgot-password/forgot-password.component";
import { ResetPasswordComponent } from "./reset-password";
import { RegisterComponent } from "./register";
import { MatCardModule, MatGridListModule, MatInputModule, MatSelectModule, MatDividerModule, MatButtonModule, MatCheckboxModule } from "@angular/material";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AuthenticationService } from "./authentication.service";
import { AuthenticationGaurd } from "./authentication.gaurd";
import { ProfileComponent } from "./profile/profile.component";
import { ProgressBarModule } from "primeng/progressbar";

@NgModule({
declarations:[
    LoginComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    RegisterComponent,
    ProfileComponent
],
imports:[
    AuthenticationRoutingModule,
    MatCardModule,
    MatGridListModule,
    ProgressBarModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    MatButtonModule,
    MatCheckboxModule
    
],
providers:[
    AuthenticationService,
    AuthenticationGaurd
]
})
export class AuthenticationModule{
    
}