import { NgModule } from "@angular/core";
import {RouterModule} from '@angular/router';
import { routes } from "./authentication.routes";


@NgModule({
declarations:[],
imports:[
    RouterModule.forChild(routes)
],
exports: [RouterModule],
providers:[]
})
export class AuthenticationRoutingModule{
    
}