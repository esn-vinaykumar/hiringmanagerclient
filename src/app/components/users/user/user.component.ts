import { Component, OnInit, AfterViewInit, ViewChild } from "@angular/core";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import { UserService } from "../user.service";

@Component({
    selector: 'user-component',
    templateUrl: './user.component.html',
    styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit, AfterViewInit {
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    dataSource = new MatTableDataSource();
    constructor(
        private userService:UserService
    ) {

    }
    displayedColumns: string[] = ['select','FirstName', 'Email', 'Role','Location'];

    ngOnInit() {

        this.userService.getAllUsers().subscribe(data=>{
            this.dataSource.data=data;
            
        })

    }
    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
    }
    applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
        
    }

}