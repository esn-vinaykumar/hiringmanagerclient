import { NgModule } from "@angular/core";
import { UserRoutingModule } from "./user.routing.module";
import { UserComponent } from "./user/user.component";
import { UserGroupComponent } from "./user-groups/userGroup.component";
import { MatTableModule, MatPaginatorModule, MatSortModule, MatCheckboxModule, MatFormFieldModule, MatInputModule } from "@angular/material";
import { UserService } from "./user.service";
import { ModalModule } from "ngx-bootstrap";

@NgModule({
    declarations:[
        UserComponent,
        UserGroupComponent
    ],
    imports:[
        UserRoutingModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        MatCheckboxModule,
        MatFormFieldModule,
        MatInputModule,
        ModalModule
    ],
    providers:[
        UserService
    ]

})
export class UserModule{
    constructor(){
        
    }
}