import { Injectable } from "@angular/core";
import { HttpConstants } from "../../constants/Http.constants";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

@Injectable()
export class UserService{
    public apiUrl:string=HttpConstants.SERVER_API_ENDPOINT;
    constructor(private httpClient:HttpClient){

    }
    getAllUsers():Observable<any>{
        return this.httpClient.get(this.apiUrl+'/api/users/getallusers')
        .pipe(
            map(res =>res),
            //catchError(this.errorHandler)
            );
    }
}