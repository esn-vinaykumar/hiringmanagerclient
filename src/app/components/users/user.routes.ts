import { Routes } from "@angular/router";
import { UserComponent } from "./user/user.component";
import { UserGroupComponent } from "./user-groups/userGroup.component";
import { AuthenticationGaurd } from "../authentication/authentication.gaurd";

export const userRoutes:Routes=[
    {path: '',component: UserComponent,data: { authenticated: true },canActivateChild:[AuthenticationGaurd]},
    {path: 'usergroups', component: UserGroupComponent,data: { authenticated: true },canActivateChild:[AuthenticationGaurd]},
    
];