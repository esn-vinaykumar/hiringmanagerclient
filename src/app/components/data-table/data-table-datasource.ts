import { DataSource } from '@angular/cdk/collections';
import { MatPaginator, MatSort } from '@angular/material';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface DataTableItem {
  id: number;
  companyInfoLogo:string;
  JobID:string;
  JobDate:any;
  title:string;
  hiringManager:string;
  location:string;
  jobOverView:string;
  jobDuties:string;
  jobSkills:string;
  jobQual:string;
  jobSoftSkills:string;
  jobTopThreeSkills:string;
  jobPersonalityTraits:string;
  jobYrsExp:string;
  jobPrevExp:string;
  jobEducation:string;
  jobCertifications:string;
  jobkeywords:string;
  jobParagraph:string;
  jobStatus:string;

}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: DataTableItem[] = [
  {
      id:1,
      "companyInfoLogo":"ABC Inc",
      "JobID":"13671",
      "JobDate":"2018/10/01",
      "hiringManager":"Adams, Henry",
      "location":"Piscataway, NJ",
      "title":"Technology Analyst",
      "jobOverView":"Improvement in Data Quality",
      "jobDuties":"Understand requirements,",
      "jobSkills":"Salesforce Lightning2",
      "jobQual":"B.tech",
      "jobSoftSkills":"Salesforce Lightning",
      "jobTopThreeSkills":"Salesforce Lightning,Visual",
      "jobPersonalityTraits":"Salesforce Lightning,Visual Force Pages Design,Apex Coding",
      "jobYrsExp":"8",
      "jobPrevExp":"7",
      "jobEducation":"B.tech",
      "jobCertifications":"Salesforce Certifications at least before 2017",
      "jobkeywords":"Salesforce Developer with Lightning",
      "jobParagraph":"The candidate must have a strong Architecture",
      "jobStatus":"Open"
  },
  {
      id:2,
      "companyInfoLogo":"MNO Inc",
      "JobID":"13672",
      "JobDate":"2018/10/18",
      "hiringManager":"Bailey, Philip James",
      "location":"San Jose, CA",
      "title":"Technology Analyst",
      "jobOverView":"Improvement in Data Quality",
      "jobDuties":"Understand requirements,",
      "jobSkills":"Salesforce Lightning2",
      "jobQual":"B.tech",
      "jobSoftSkills":"Salesforce Lightning",
      "jobTopThreeSkills":"Salesforce Lightning,Visual",
      "jobPersonalityTraits":"Salesforce Lightning,Visual Force Pages Design,Apex Coding",
      "jobYrsExp":"8",
      "jobPrevExp":"7",
      "jobEducation":"B.tech",
      "jobCertifications":"Salesforce Certifications at least before 2017",
      "jobkeywords":"Salesforce Developer with Lightning",
      "jobParagraph":"The candidate must have a strong Architecture",
      "jobStatus":"Closed"
  },
  {
      id:3,
      "companyInfoLogo":"XYZ Inc",
      "JobID":"13673",
      "JobDate":"2018/05/10",
      "hiringManager":"Carlyle, Thomas",
      "location":"New York, NY",
      "title":"Technology Analyst",
      "jobOverView":"Improvement in Data Quality",
      "jobDuties":"Understand requirements,",
      "jobSkills":"Salesforce Lightning2",
      "jobQual":"B.tech",
      "jobSoftSkills":"Salesforce Lightning",
      "jobTopThreeSkills":"Salesforce Lightning,Visual",
      "jobPersonalityTraits":"Salesforce Lightning,Visual Force Pages Design,Apex Coding",
      "jobYrsExp":"8",
      "jobPrevExp":"7",
      "jobEducation":"B.tech",
      "jobCertifications":"Salesforce Certifications at least before 2017",
      "jobkeywords":"Salesforce Developer with Lightning",
      "jobParagraph":"The candidate must have a strong Architecture",
      "jobStatus":"Closed"
  },
  {
      id:4,
      "companyInfoLogo":"ZMN Inc",
      "JobID":"13674",
      "JobDate":"2018/03/10",
      "hiringManager":"Darwin, Charles",
      "location":"Hoboken, NJ",
      "title":"Technology Analyst",
      "jobOverView":"Improvement in Data Quality",
      "jobDuties":"Understand requirements,",
      "jobSkills":"Salesforce Lightning2",
      "jobQual":"B.tech",
      "jobSoftSkills":"Salesforce Lightning",
      "jobTopThreeSkills":"Salesforce Lightning,Visual",
      "jobPersonalityTraits":"Salesforce Lightning,Visual Force Pages Design,Apex Coding",
      "jobYrsExp":"8",
      "jobPrevExp":"7",
      "jobEducation":"B.tech",
      "jobCertifications":"Salesforce Certifications at least before 2017",
      "jobkeywords":"Salesforce Developer with Lightning",
      "jobParagraph":"The candidate must have a strong Architecture",
      "jobStatus":"Open"
  },
  {
      id:5,
      "companyInfoLogo":"OCQ Inc",
      "JobID":"13675",
      "JobDate":"2018/09/11",
      "hiringManager":"Eisenstein",
      "location":"Piscataway, NJ",
      "title":"Technology Analyst",
      "jobOverView":"Improvement in Data Quality",
      "jobDuties":"Understand requirements,",
      "jobSkills":"Salesforce Lightning2",
      "jobQual":"B.tech",
      "jobSoftSkills":"Salesforce Lightning",
      "jobTopThreeSkills":"Salesforce Lightning,Visual",
      "jobPersonalityTraits":"Salesforce Lightning,Visual Force Pages Design,Apex Coding",
      "jobYrsExp":"8",
      "jobPrevExp":"7",
      "jobEducation":"B.tech",
      "jobCertifications":"Salesforce Certifications at least before 2017",
      "jobkeywords":"Salesforce Developer with Lightning",
      "jobParagraph":"The candidate must have a strong Architecture",
      "jobStatus":"Open"
  }
];

/**
 * Data source for the DataTable view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class DataTableDataSource extends DataSource<DataTableItem> {
  data: DataTableItem[] = EXAMPLE_DATA;

  constructor(private paginator: MatPaginator, private sort: MatSort) {
    super();
  }

  filterPredicate =(data: DataTableItem, filters: string) => {
    const matchFilter = [];
    const filterArray = filters.split(',');
     // Or  specifics columns =>
    // const columns = [data.JobID, data.JobDate, data.title,data.companyInfoLogo, data.hiringManager,
    //    data.location, data.jobStatus];
    // Or if you don't want to specify specifics columns =>
     const columns = (<any>Object).values(data);
    
    //Main loop
    filterArray.forEach(filter => {
      const customFilter = [];
      columns.forEach(column => customFilter.push(column.toLowerCase().includes(filter)));
      matchFilter.push(customFilter.some(Boolean)); // OR
    });
    return matchFilter.every(Boolean); // AND
  }
  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<DataTableItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    // Set the paginators length
    this.paginator.length = this.data.length;

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: DataTableItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: DataTableItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'JobID': return compare(a.JobID, b.JobID, isAsc);
        case 'JobDate': return compare(a.JobDate, b.JobDate, isAsc);
        case 'title': return compare(a.title, b.title, isAsc);
        case 'companyInfoLogo': return compare(a.companyInfoLogo, b.companyInfoLogo, isAsc);
        case 'hiringManager': return compare(a.JobID, b.JobID, isAsc);
        case 'location': return compare(a.title, b.title, isAsc);
        case 'jobStatus': return compare(a.JobID, b.JobID, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a, b, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
