import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
export interface DialogData {
    animal: string;
    name: string;
  }
@Component({
  selector: 'deletejob-component',
  templateUrl: './deletejob.component.html',
})
  export class DeleteJobDialog {
  
    constructor(
      public dialogRef: MatDialogRef<DeleteJobDialog>,
      @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
  
    onNoClick(): void {
      this.dialogRef.close();
    }
    close(){
      this.dialogRef.close(false);
    }
    ok(){
      this.dialogRef.close(true);
    }
  
  }