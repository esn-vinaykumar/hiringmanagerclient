import { Component, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from "@angular/material";
import { DataTableDataSource } from "../../data-table/data-table-datasource";
import { Router } from "@angular/router";
import { FormControl } from "@angular/forms";
import { filter } from "rxjs/operators";
import { JobService } from "./JobService";
import { JobDescriptionService } from "../../../jobs/JobDescriptionService";
import { IJobDescription } from "../../../jobs/IJobDescription";
import { DeleteJobDialog } from "./DeleteJobDialog";
import { AuthenticationService } from "../../authentication/authentication.service";
import { DatePipe } from "@angular/common";
export interface JobStatusList {
    value: string;
    viewValue: string;
}

@Component({
    selector: 'job-list-component',
    templateUrl: './joblist.component.html',
    styleUrls: ['./joblist.component.scss']
})
export class JobListComponent implements OnInit,AfterViewInit {

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    animal: string;
    loggedInfo:any;
    searchJobFilter:any;
  name: string;
    //dataSource: DataTableDataSource;
    dataSource = new MatTableDataSource();
    displayedColumns = ['action','JobID','CreatedDate', 'Title', 'CompanyName', 'user.UserName', 'Location', 'JobStatus'];
    jobs: JobStatusList[] = [
        { value: 'All', viewValue: 'All Jobs' },
        { value: '0', viewValue: 'My Jobs' },
        { value: 'Open', viewValue: 'All Open Jobs' },
        { value: 'Close', viewValue: 'All Closed Jobs' }
    ];
    
    ngOnInit() {
        this.loggedInfo=this.authService.getLoggedInfo();
        //this.dataSource = new DataTableDataSource(this.paginator, this.sort);
        this.jobDescService.getAllJobs().subscribe(data=>{
            this.dataSource.data=data;
            this.dataSource.filterPredicate =
            (data:any, filters: string) => {
              const matchFilter = [];
              const filterArray = filters.split(',');
              const columns = [data.JobID, data.CreatedDate, data.Title,data.CompanyName, data.user.UserName, data.Location, data.JobStatus,];
              // Or if you don't want to specify specifics columns =>
              // const columns = (<any>Object).values(data);
              
              //Main loop
              filterArray.forEach(filter => {
                const customFilter = [];
                columns.forEach(column => customFilter.push(column.toLowerCase().includes(filter)));
                matchFilter.push(customFilter.some(Boolean)); // OR
              });
              return matchFilter.every(Boolean); // AND
            }
            this.dataSource.sortingDataAccessor=function(item: any, path: string): any {
                return path.split('.')
                .reduce((accumulator: any, key: string) => {
                  return accumulator ? accumulator[key] : undefined;
                }, item);
            }
        })
        
    }
    constructor(private router:Router,
        private dialog:MatDialog,
        private jobservice:JobService,
        private authService:AuthenticationService,
    private jobDescService:JobDescriptionService){
        this.searchJobFilter={
            fromDate:'',
            toDate:''
        }
    }
    

    ngAfterViewInit() {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    
    applyFilter(filterValue: string) {
        filterValue = filterValue.trim();
        filterValue = filterValue.toLowerCase();
        this.dataSource.filter = filterValue;
        
    }
    addJob() {
        this.router.navigate(['/createjobdescription']);

    }
    editJob() {

    }
    fromDatechange(){

        
        if(this.searchJobFilter.fromDate && this.searchJobFilter.toDate){
            var payLoad={
                startDate:new DatePipe("en-US").transform(this.searchJobFilter.fromDate, 'yyyy-MM-dd, h:mm a'),
                endDate:new DatePipe("en-US").transform(this.searchJobFilter.toDate, 'yyyy-MM-dd, h:mm a')
    
            }
            this.jobDescService.getJobDateRange(payLoad).subscribe(data=>{
                this.dataSource.data=data;
            })
        }
    }
    deleteJob(id:number){
        const dialogRef = this.dialog.open(DeleteJobDialog, {
            width: '400px'
          });
      
          dialogRef.afterClosed().subscribe(result => {
            console.log('The dialog was closed'+result);
            if(result){
                var payLoad = {
                    id: id + ""
                }
                this.jobDescService.deleteJobById
                this.jobDescService.deleteJobById(payLoad)
                    .subscribe(data => {
                        this.jobDescService.getAllJobs().subscribe(data=>{
                            this.dataSource.data=data;
                        })
                    })
            }
          });
        }
    changeByJobStatus(filterValue: string){

    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    if(filterValue=='all'){
        this.jobDescService.getAllJobs().subscribe(data=>{
            this.dataSource.data=data;
        })
        this.dataSource.filter = '';
    }
    else if(filterValue=="0"){
        var payLoad={
            id:this.loggedInfo.id

        }
        this.jobDescService.getUserJobs(payLoad).subscribe(data=>{
            this.dataSource.data=data;
        })
    }
    else {
        this.jobDescService.getAllJobs().subscribe(data=>{
            this.dataSource.data=data;
            this.dataSource.filter = filterValue;
        })
        
    }
    
    }
    


}
