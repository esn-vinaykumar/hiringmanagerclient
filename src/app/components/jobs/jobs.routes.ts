import { Routes } from "@angular/router";
import { JobListComponent } from "./joblist";

export const routes: Routes = [
    {path: '',component: JobListComponent},
    {path: 'jobgrid',component: JobListComponent,data: { authenticated: true }}
   ];