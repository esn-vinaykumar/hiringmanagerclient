import { NgModule } from "@angular/core";
import { JobListComponent } from "./joblist";
import { JobRoutingModule } from "./jobs.routing.module";


@NgModule({
    declarations:[
        JobListComponent
    ],
    imports:[
        JobRoutingModule,
    ]

})
export class JobsModule{}