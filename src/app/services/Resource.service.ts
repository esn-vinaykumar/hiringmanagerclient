import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

export class Resource {
    id: number;
}
export interface Serializer {
    fromJson(json: any): Resource;
    toJson(resource: Resource): any
}

@Injectable()
export class ResourceService<T extends Resource>{
    baseUrl: string = "http://localhost:54743";
    constructor(
        private httpClient: HttpClient,
        private endPoint: string,
        private serializer: Serializer) { }

    public create(item: T): Observable<T> {
        return this.httpClient
            .post(`${this.baseUrl}/${this.endPoint}`, this.serializer.toJson(item))
            .map(data => this.serializer.fromJson(data) as T);
    }
    public update(item: T): Observable<T> {
        return this.httpClient
            .put(`${this.baseUrl}/${this.endPoint}/${item.id}`,
                this.serializer.toJson(item))
            .map(data => this.serializer.fromJson(data) as T);
    }
    public read(id:number):Observable<T>{
        return this.httpClient
        .get(`${this.baseUrl}/${this.endPoint}/${id}`)
        .map((data:any) => this.serializer.fromJson(data) as T);
    }
    // public list(queryOptions: QueryOptions): Observable<T[]> {
    //     return this.httpClient
    //       .get(`${this.baseUrl}/${this.endPoint}?${queryOptions.toQueryString()}`)
    //       .map((data: any) => this.convertData(data.items));
    //   }

    delete(id: number) {
        return this.httpClient
          .delete(`${this.baseUrl}/${this.endPoint}/${id}`);
      }



      private convertData(data: any): T[] {
        return data.map(item => this.serializer.fromJson(item));
      }




}